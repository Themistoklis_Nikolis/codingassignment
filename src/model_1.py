import Embeddings
import os
from pathlib import Path
import pickle
import spacy
import keras
import numpy as np
from keras.layers import Input, Dense, LSTM, Dropout
from keras.models import Model
from sklearn.utils.extmath import softmax

path = os.path.dirname(os.path.realpath(__file__)[:-3])
path = str(Path(path).parent)

def main(train = False):
    with open(path + '/data/word_list.pkl', 'rb') as f:
        word_list = pickle.load(f)
    embedding_matrix = np.load(path + '/data/embedding_matrix.npy')
    context_ar = np.load(path + '/data/context_ar.npy')
    question_ar = np.load(path + '/data/question_ar.npy')
    mask_ar = np.load(path + '/data/mask_ar.npy')
    context_ar_val = np.load(path + '/data/context_ar_val.npy')
    question_ar_val = np.load(path + '/data/question_ar_val.npy')
    mask_ar_val = np.load(path + '/data/mask_ar_val.npy')
    emclen = int(Embeddings.max_con_len)
    emqlen = int(Embeddings.max_q_len)
    wvc = int(Embeddings.wvc)
    # The model is a simple LSTM-LSTM-FF-FF Neural network that outputs/predicts an array of max context length
    # with sigmoid values, from which the largest is considered the one word answer
    context_input = Input(shape=(emclen,), name='context_input')
    question_input = Input(shape=(emqlen,), name='question_input')

    # Embedding layers for context and question
    embedding_layer = keras.layers.Embedding(len(word_list), wvc, weights = [embedding_matrix], input_length = emclen,trainable=False)
    embedding_layer_question = keras.layers.Embedding(len(word_list), wvc,weights=[embedding_matrix],input_length = emqlen,trainable=False)
    embedded_sequences_context = embedding_layer(context_input)
    embedded_sequences_question = embedding_layer_question(question_input)

    # Input the Question
    encoder_outputs, state_h, state_c = LSTM(256, return_state=True)(embedded_sequences_question)

    # Input the Context
    encoder_states = [state_h, state_c]
    context_encoder = LSTM(256, dropout = 0.2)(embedded_sequences_context, initial_state=encoder_states)

    # Feed Forward Layers
    intermediate = Dense(emclen, activation = 'sigmoid')(context_encoder)
    #drop = Dropout(0.2, input_shape=(emclen,))(intermediate)
    predictions = Dense(emclen, activation = 'sigmoid', name = 'main_output')(intermediate)
    #finalOut = Dense(emqlen, activation = 'relu')(predictions)
    model = Model(inputs = [context_input, question_input], outputs = predictions)
    model.compile(optimizer = 'sgd',loss = 'categorical_crossentropy',metrics = ['accuracy'])
    if(train):
        print("Training model.")
        model.fit( {'context_input':context_ar,'question_input':question_ar},{'main_output':mask_ar}, epochs = 10, batch_size = 512)
        model.save_weights(path + '/data/weights.h5')

    model.load_weights(path + '/data/weights.h5')
    pred = model.predict({'context_input':context_ar_val, 'question_input':question_ar_val}, batch_size=1)
    np.save(path + '/data/preds/predictions.npy')
    position = []
    for prediction in pred:
        position.append = np.argmax(prediction)
    
if __name__ == "__main__":
    main(train = True)