import Helpers 
from pathlib import Path 
import pickle 
import os
import json
import numpy as np
import tqdm
import ast
from itertools import repeat, chain, islice



path = os.path.dirname(os.path.realpath(__file__)[:-3])
path = str(Path(path).parent)

print(path)
with open(path + '/data/word_idx_lookup.pkl', 'rb') as f:
    word_idx_lookup = pickle.load(f)

with open(path + '/data/example.txt') as f:
    examples = ast.literal_eval(f.read())

glove_size = 50
num_matched = 0
embedding_matrix = np.zeros((len(word_idx_lookup), int(glove_size)))
embeddings_dict = {}
wvc = 50

with open(path + '/data/glove.6B.' + str(wvc) + 'd.txt',encoding='utf-8') as gf:
            for line in gf:
                values = line.split(' ')
                word = values[0]
                coefs = np.asarray(values[1:],dtype='float32')
                embeddings_dict[word] = coefs

for word in tqdm.tqdm(word_idx_lookup):
            if(word.lower() in embeddings_dict):
                embedding_matrix[word_idx_lookup[word],:] = embeddings_dict[word.lower()]
                num_matched += 1

np.save(path + '/data/embedding_matrix' ,embedding_matrix)

num_uknwn = 0
train_percent = 0.88
shuffle_ex = True

max_con_len = 400
max_q_len = 30

indices = Helpers.get_train_indices(examples, max_con_len, max_q_len)
num_training_samples = len(indices)

if shuffle_ex == True:
    np.random.shuffle(indices)

split_point = int(num_training_samples * train_percent)

train_count = split_point

train_inds = indices[:split_point]

xval_inds = indices[split_point:]

#For max context and question length we take the minimum of the 95 percentile of their lengths and 
#the hyperparameter we chose for each. The lengths are 232 and 18 respectively!
max_con_len = min([np.percentile(np.array([len(example['context_p']) for example in examples]), 95).round() + 1, max_con_len])
max_q_len = min([np.percentile(np.array([len(example['question_p']) for example in examples]), 95).round() + 1, max_q_len])                                  

#Create Training and Evaluation Datasets
Helpers.train_val_creation(examples, word_idx_lookup, train_inds, max_con_len, max_q_len, path)
Helpers.train_val_creation(examples, word_idx_lookup, xval_inds, max_con_len, max_q_len, path, val = "_val")
