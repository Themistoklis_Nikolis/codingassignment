import torch
import nltk
from nltk.stem.porter import PorterStemmer
import json
import spacy
from nltk.corpus import stopwords
import numpy as np
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
#import spacyspacy.prefer_gpu()
import random
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset
from typing import Tuple
import re
import copy
import time
import itertools
from itertools import zip_longest
import math
import tqdm
import sys

spacy.prefer_gpu()
sp = spacy.load('en_core_web_sm')
s = set(stopwords.words('english'))

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = 'cpu'

def chunker(iterable, chunksize):
    return zip(*[iter(iterable)] * chunksize)
 
def chunker_longest(iterable, chunksize):
    return zip_longest(*[iter(iterable)] * chunksize)

def tokenize(sentence):
    return nltk.word_tokenize(sentence)

def lemmatize(phrase):
    return " ".join([word.lemma_ for word in sp(phrase)])

def tokenize_sub(sentences):
    sentences = sentences.replace("—",' ')
    sentences = sentences.replace("-",' ')
    sentences = sentences.replace("'s",'')
    lwrd_sbd = re.sub("[()—:-;'.,!?\\-]", '', sentences.lower()).split('\n')
    tknz = tokenize(lwrd_sbd.pop())
    lwrd_sbd = re.sub("[()—:-;'.,!?\\-]", '', sentences.lower()).split('\n')
    return tknz

def process(sentences):
    lwrd_sbd = re.sub("[():;'.,!?\\-]", '', sentences.lower()).split('\n')
    tknz = tokenize(lwrd_sbd.pop())
    lmm = [lemmatize(tkn) for tkn in tknz]
    return lmm
    
def create_mask():
    masks = []
    for (an_start, an_end, answer) in zip(answer_starts, answer_ends, answers):
        mask = []
        for pos, token in enumerate(context):
            if pos >= int(an_start) and pos <= int(an_end):
                mask.append(token)
        mask = "".join(mask)
        tmask = tokenize_sub(answer)
        j = 0
        if tmask is not None: 
            for j in range(len(tmask)):
                k = 0
                
                for chunk in chunker_longest(context_p[j:], len(tmask)):
                    i = k*len(tmask) + j
                    k += 1
                    if (np.array(chunk) == np.array(tmask)).all():
                        mask = ['0' * i + '1' * len(tmask) + '0' * (len(context_p) - len(tmask) - i)]
                        break
                j += 1
        masks.append(mask)
    return masks

with open('/home/themisnik/Downloads/dev-v2.0.json', 'r', encoding = 'utf-8') as f:
    raw_data = json.load(f)
    examples = []
    num = 1
    for document in raw_data['data']:
        for paragraph in document['paragraphs']:
            context = paragraph['context']
            context_p = tokenize_sub(context)
            for qna in paragraph['qas']:
                question = qna['question']
                question_p = tokenize_sub(question)
                id_ = qna['id']
                label = 1 if qna.get("is_impossible", False) else 0
                answers, answers_t, answer_starts, answer_ends = [], [], [], []
                for answer in qna['answers']:
                    answer_start = answer['answer_start']
                    answer_end = answer_start + len(answer['text'])
                    answers.append(answer['text'])
                    answer_starts.append(answer_start)
                    answer_ends.append(answer_end)
                    answers_t.append(tokenize_sub(answer['text']))
                example = {
                    'id': id_,
                    'idx': len(examples),
                    'context': context,
                    'question': question,
                    'context_p': context_p,
                    'question_p': question_p,
                    'impossible': label,
                    'answers': answers,
                    'answers_t': answers_t,
                    'answer_starts': answer_starts,
                    'answer_ends': answer_ends,
                    'mask': create_mask()
                    }
                examples.append(example)
        num += 1 
        print(num)
        if num == 1000:
            break

for example in examples:
    if len(example['mask']) == 0:
        example['mask'] = '0' * len(example['context_p']) 
    for pos, mask in enumerate(example['mask']):
        if not mask[0].isdecimal():
            example['mask'][pos] = '0' * len(example['context_p'])

for n, example in enumerate(examples):
    if len(example['mask']) > 1 and '0' not in list(example['mask'][0][0]):
        print(n) 
            
vocab = {}
for example in examples:
    for word in example['context_p']:
        if word not in vocab:
            vocab[word] = 1 
        else:
             vocab[word] += 1
    for word in example['question_p']:
        if word not in vocab:
            vocab[word] = 1
        else:
            vocab[word] += 1
    for answer in example['answers_t']:
        for word in answer:
            if word not in vocab:
                vocab[word] = 1 
            else:
                vocab[word] += 1
word_list = ['_UNK_'] + sorted(vocab, key = vocab.get, reverse = True)
word_idx_lookup = dict([(x,y) for (y,x) in enumerate(word_list)])

glove_size = 50
num_matched = 0
embedding_matrix = np.zeros( ( len(word_idx_lookup), int(glove_size) ) )
embeddings_dict = {}

with open('/home/themisnik/Downloads/glove.6B/glove.6B.50d.txt',encoding='utf-8') as gf:
            for line in gf:
                values = line.split(' ')
                word = values[0]
                coefs = np.asarray(values[1:],dtype='float32')
                embeddings_dict[word] = coefs

embedding_matrix.size
for word in tqdm.tqdm(word_idx_lookup):
            if(word.lower() in embeddings_dict):
                embedding_matrix[word_idx_lookup[word],:] = \
                        embeddings_dict[word.lower()]
                num_matched += 1

pad_number = 0 
num_uknwn = 0
train_percent = 0.88
word_vec_size = 50
pad_zero = True
shuffle_ex = True

def get_train_indices():
    contextLengths = []
    questionLengths = []
    for example in examples:
        contextLengths.append(len(example['context_p']))
        questionLengths.append(len(example['question_p']))
    contextLengths = np.array(contextLengths)
    questionLengths = np.array(questionLengths)
    indices = np.nonzero(contextLengths <= max_con_len)[0]
    qIndices = np.nonzero(questionLengths <= max_q_len)[0]
    indices = np.intersect1d( indices, qIndices )
    return indices

max_con_len = 750
max_q_len = 40

indices = get_train_indices()
num_training_samples = len(indices)

if shuffle_ex == True:
    np.random.shuffle(indices)

split_point = int(num_training_samples * train_percent)

train_count = split_point

train_inds = indices[:split_point]

xval_inds = indices[split_point:]

def lineToWordIndices(context_p):
    indices = []
    global num_uknwn
    for word in context_p:
        if word in word_idx_lookup:
            indices.append(word_idx_lookup[word])
        else:
            num_uknwn += 1
            indices.append(0)
    return indices

def countTrainingExamples(self, fileName ):
        lineCount = 0
        with open(fileName,"r",encoding="utf-8") as cf:
            for line in cf:
                lineCount += 1
        self.numTrainingExamples = lineCount
        return self.numTrainingExamples

def generate_train_val_dataset():
    for i in indices:
        context = lineToWordIndices(i)

def to_int(ls):
    try:
        out = int(ls)
    except ValueError:
        out = 0
    return out

max_con_len = max([max(len(example['context_p']) for example in examples), max_con_len])
max_q_len = max([max(len(example['question_p']) for example in examples), max_q_len])

context_ar, question_ar, mask_ar = [], [], []

for example in examples:
    context_line = lineToWordIndices(example['context_p'])
    while len(context_line) < max_con_len:
        context_line.append(pad_number)
    
    question_line =  lineToWordIndices(example['question_p'])
    while len(question_line) < max_q_len:
        question_line.append(pad_number)
    
    for mask in example['mask']:
        mask_line = [list(map(int, list(mask[0].strip())))]
    mask_line = mask_line[0]
    while len(mask_line) < max_con_len + max_q_len:
        mask_line.append(0)
    print(len(mask_line))
    text_line = lineToWordIndices(example['context'].strip().split())
    context_ar.append(context_line)
    question_ar.append(question_line)
    mask_ar.append(mask_line)
    #print(1 in mask_line)

context_ar = np.array(context_ar)
question_ar = np.array(question_ar)
mask_ar = np.array(mask_ar)

train_set, ev_set, train_set_masks, ev_set_masks = [], [], [], []
trainset = np.zeros((790, 1))
train_set_masks = []
for z, i in enumerate(train_inds):
    print(z)
    embed, train_set_mask = [], []
    len_c = len(examples[i]['context_p'])
    len_q = len(examples[i]['question_p'])
    for mask in examples[i]['mask']:
        train_set_mask.append(mask)
    train_set_masks.append(list(train_set_mask))
    for item in examples[i]['context_p']:
        embed.append(word_idx_lookup[item])
    con_em = np.concatenate((np.stack(embed, axis = 0).reshape(-1,1), np.zeros((max_con_len - len_c)).reshape(-1, 1)), axis = 0)
    embed = []
    for item in examples[i]['question_p']:
        embed.append(word_idx_lookup[item])
    q_em = np.concatenate((np.stack(embed, axis = 0).reshape(-1,1), np.zeros((max_q_len - len_q)).reshape(-1, 1)), axis = 0)
    trainset = np.concatenate((trainset, np.concatenate((con_em, q_em), axis = 0)), axis = -1)

len(train_set_masks)

class TransformerModel(nn.Module):

    def __init__(self, ntoken: int, d_model: int, nhead: int, d_hid: int,
                 nlayers: int, dropout: float = 0.5):
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, d_model)
        self.d_model = d_model
        self.decoder = nn.Linear(d_model, ntoken)

        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src: Tensor, src_mask: Tensor) -> Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        src = self.encoder(src) * math.sqrt(self.d_model)
        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, src_mask)
        output = self.decoder(output)
        return output


def generate_square_subsequent_mask(sz: int) -> Tensor:
    """Generates an upper-triangular matrix of -inf, with zeros on diag."""
    return torch.triu(torch.ones(sz, sz) * float('-inf'), diagonal=1)

class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x: Tensor) -> Tensor:
        """
        Args:
            x: Tensor, shape [seq_len, batch_size, embedding_dim]
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)

def data_process(raw_text_iter: dataset.IterableDataset) -> Tensor:
    """Converts raw text into a flat Tensor."""
    data = [torch.tensor(vocab(tokenizer(item)), dtype=torch.long) for item in raw_text_iter]
    return torch.cat(tuple(filter(lambda t: t.numel() > 0, data)))

bptt = 100

def get_batch(source, masks, i: int) -> Tuple[Tensor, Tensor]:
    """
    Args:
        source: Tensor, shape [full_seq_len, batch_size]
        i: int

    Returns:
        tuple (data, target), where data has shape [seq_len, batch_size] and
        target has shape [seq_len * batch_size]
    """
    seq_len = min(bptt, len(source) - 1 - i)
    data = torch.as_tensor(source[i:i+seq_len], dtype = torch.long).to(device)
    target = torch.as_tensor(masks[i+1:i+1+seq_len], dtype = torch.long).reshape(-1).to(device)
    return data, target

ntokens = 1000# len(vocab)  # size of vocabulary
emsize = 10  # embedding dimension
d_hid = 20  # dimension of the feedforward network model in nn.TransformerEncoder
nlayers = 1  # number of nn.TransformerEncoderLayer in nn.TransformerEncoder
nhead = 2  # number of heads in nn.MultiheadAttention
dropout = 0.2  # dropout probability
model = TransformerModel(ntokens, emsize, nhead, d_hid, nlayers, dropout).to(device)

criterion = nn.CrossEntropyLoss()
lr = 0.95  # learning rate
optimizer = torch.optim.SGD(model.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)
bptt = 128
trainset = trainset.transpose()
trainset.shape
def train(model: nn.Module) -> None:
    model.train()  # turn on train mode
    total_loss = 0.
    log_interval = 100
    start_time = time.time()
    src_mask = generate_square_subsequent_mask(bptt).to(device)

    num_batches = len(trainset) // bptt
    for batch, i in enumerate(range(0, trainset.shape[0] - 1, bptt)):
        data, targets = get_batch(trainset, mask_ar, i)
        batch_size = data.size(0)
        if batch_size != bptt:  # only on last batch
            src_mask = src_mask[:batch_size, :batch_size]
        output = model(data, src_mask)
        loss = criterion(output.view(-1, ntokens), targets)

        model.parameters()

        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        if batch % log_interval == 0 and batch > 0:
            lr = scheduler.get_last_lr()[0]
            ms_per_batch = (time.time() - start_time) * 1000 / log_interval
            cur_loss = total_loss / log_interval
            ppl = math.exp(cur_loss)
            print(f'| epoch {epoch:3d} | {batch:5d}/{num_batches:5d} batches | '
                  f'lr {lr:02.2f} | ms/batch {ms_per_batch:5.2f} | '
                  f'loss {cur_loss:5.2f} | ppl {ppl:8.2f}')
            total_loss = 0
            start_time = time.time()

torch.cuda.empty_cache()