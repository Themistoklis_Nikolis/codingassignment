import torch
import nltk
from nltk.stem.porter import PorterStemmer
import json
import spacy
from nltk.corpus import stopwords
import numpy as np
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
import spacyspacy.prefer_gpu()
import random
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset
from typing import Tuple
import re
import copy
import time
import itertools
from itertools import zip_longest
import math
import tqdm
import sys

spacy.prefer_gpu()
sp = spacy.load('en_core_web_sm')
s = set(stopwords.words('english'))

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = 'cpu'

train_set, ev_set, train_set_masks, ev_set_masks = [], [], [], []
trainset = np.zeros((790, 1))
train_set_masks = []
for z, i in enumerate(train_inds):
    print(z)
    embed, train_set_mask = [], []
    len_c = len(examples[i]['context_p'])
    len_q = len(examples[i]['question_p'])
    for mask in examples[i]['mask']:
        train_set_mask.append(mask)
    train_set_masks.append(list(train_set_mask))
    for item in examples[i]['context_p']:
        embed.append(word_idx_lookup[item])
    con_em = np.concatenate((np.stack(embed, axis = 0).reshape(-1,1), np.zeros((max_con_len - len_c)).reshape(-1, 1)), axis = 0)
    embed = []
    for item in examples[i]['question_p']:
        embed.append(word_idx_lookup[item])
    q_em = np.concatenate((np.stack(embed, axis = 0).reshape(-1,1), np.zeros((max_q_len - len_q)).reshape(-1, 1)), axis = 0)
    trainset = np.concatenate((trainset, np.concatenate((con_em, q_em), axis = 0)), axis = -1)

len(train_set_masks)

sample = [] 
for i in range(len(train_set_masks)):
    sample.append((torch.from_numpy(trainset[:,i]), torch.from_numpy(np.array(mask_ar[i]))))

np.concatenate((con_em, q_em), axis = 0).shape
trainset = np.stack(train_set)
trainset.shape
np.save('/home/themisnik/Downloads/trainset.npy',trainset)
trainset = np.load('/home/themisnik/Downloads/trainset.npy')
sys.getsizeof(train_set)
trainset = trainset.reshape(790,50,-1)
trainset = np.transpose(trainset, (2,0,1))

vectorizer = TfidfVectorizer(
    stop_words='english', min_df=5, max_df=.5, ngram_range=(1,3))

for example in examples:
    for word in example['context']:
        corpus.append(word)
    for word in example['question']:
        corpus.append(word)

tfidf = vectorizer.fit_transform(corpus)
len(vocab)
len(embeddings_dict)
embedding_matrix.shape
np.sum(tfidf, axis = 1).nonzero()
tfidf_emb_sparse = tfidf.dot(embedding_matrix)
word_idx_lookup
# sample2 = random.sample(examples2, k = 1000)

# lemmas = [lemmatize(example['question']) for example in sample]
# len(examples)

# sentences = re.sub("[.,!?\\-]", '', lemma.lower()).split('\n') for lemma in lemmas # filter '.', ',', '?', '!'
# word_list = list(set(" ".join([" ".join(sentence) for sentence in sentences] ).split()))

# len(word_list)
# word_dict = {'[PAD]': 0, '[CLS]': 1, '[SEP]': 2, '[MASK]': 3}
# for i, w in enumerate(word_list):
#    word_dict[w] = i + 4
#    number_dict = {i: w for i, w in enumerate(word_dict)}
#    vocab_size = len(word_dict)

class TransformerModel(nn.Module):

    def __init__(self, ntoken: int, d_model: int, nhead: int, d_hid: int,
                 nlayers: int, dropout: float = 0.5):
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, d_model)
        self.d_model = d_model
        self.decoder = nn.Linear(d_model, ntoken)

        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src: Tensor, src_mask: Tensor) -> Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        src = self.encoder(src) * math.sqrt(self.d_model)
        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, src_mask)
        output = self.decoder(output)
        return output


def generate_square_subsequent_mask(sz: int) -> Tensor:
    """Generates an upper-triangular matrix of -inf, with zeros on diag."""
    return torch.triu(torch.ones(sz, sz) * float('-inf'), diagonal=1)

class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x: Tensor) -> Tensor:
        """
        Args:
            x: Tensor, shape [seq_len, batch_size, embedding_dim]
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)

def data_process(raw_text_iter: dataset.IterableDataset) -> Tensor:
    """Converts raw text into a flat Tensor."""
    data = [torch.tensor(vocab(tokenizer(item)), dtype=torch.long) for item in raw_text_iter]
    return torch.cat(tuple(filter(lambda t: t.numel() > 0, data)))


def batchify(data: Tensor, bsz: int) -> Tensor:
    """Divides the data into bsz separate sequences, removing extra elements
    that wouldn't cleanly fit.

    Args:
        data: Tensor, shape [N]
        bsz: int, batch size

    Returns:
        Tensor of shape [N // bsz, bsz]
    """
    seq_len = data.size(0) // bsz
    data = data[:seq_len * bsz]
    data = data.view(bsz, seq_len).t().contiguous()
    return data.to(device)
context_ar
batch_size = 20
eval_batch_size = 10
train_data = batchify(train_data, batch_size)  # shape [seq_len, batch_size]
val_data = batchify(val_data, eval_batch_size)
test_data = batchify(test_data, eval_batch_size)

train_data_con = batchify(torch.from_numpy(context_ar), 1000)
train_data_q = batchify(torch.from_numpy(question_ar), 1000)

torch.from_numpy(context_ar)

bptt = 100
def get_batch(source: Tensor, i: int) -> Tuple[Tensor, Tensor]:
    """
    Args:
        source: Tensor, shape [full_seq_len, batch_size]
        i: int

    Returns:
        tuple (data, target), where data has shape [seq_len, batch_size] and
        target has shape [seq_len * batch_size]
    """
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].reshape(-1)
    return data, target

def get_batch(source, masks, i: int) -> Tuple[Tensor, Tensor]:
    """
    Args:
        source: Tensor, shape [full_seq_len, batch_size]
        i: int

    Returns:
        tuple (data, target), where data has shape [seq_len, batch_size] and
        target has shape [seq_len * batch_size]
    """
    seq_len = min(bptt, len(source) - 1 - i)
    data = torch.as_tensor(source[i:i+seq_len], dtype = torch.long).to(device)
    target = torch.as_tensor(masks[i+1:i+1+seq_len], dtype = torch.long).reshape(-1).to(device)
    return data, target

torch.as_tensor(trainset)
get_batch(trainset, mask_ar, 100)

train_data = trainset

ntokens = len(vocab)  # size of vocabulary
emsize = 50  # embedding dimension
d_hid = 20  # dimension of the feedforward network model in nn.TransformerEncoder
nlayers = 2  # number of nn.TransformerEncoderLayer in nn.TransformerEncoder
nhead = 2  # number of heads in nn.MultiheadAttention
dropout = 0.2  # dropout probability
model = TransformerModel(ntokens, emsize, nhead, d_hid, nlayers, dropout).to(device)

criterion = nn.CrossEntropyLoss()
lr = 5.0  # learning rate
optimizer = torch.optim.SGD(model.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)
bptt = 100
trainset = trainset.transpose()
trainset.shape
def train(model: nn.Module) -> None:
    model.train()  # turn on train mode
    total_loss = 0.
    log_interval = 100
    start_time = time.time()
    src_mask = generate_square_subsequent_mask(bptt).to(device)

    num_batches = len(trainset) // bptt
    for batch, i in enumerate(range(0, trainset.shape[0] - 1, bptt)):
        data, targets = get_batch(trainset, mask_ar, i)
        batch_size = data.size(0)
        if batch_size != bptt:  # only on last batch
            src_mask = src_mask[:batch_size, :batch_size]
        output = model(data, src_mask)
        loss = criterion(output.view(-1, ntokens), targets)

        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        if batch % log_interval == 0 and batch > 0:
            lr = scheduler.get_last_lr()[0]
            ms_per_batch = (time.time() - start_time) * 1000 / log_interval
            cur_loss = total_loss / log_interval
            ppl = math.exp(cur_loss)
            print(f'| epoch {epoch:3d} | {batch:5d}/{num_batches:5d} batches | '
                  f'lr {lr:02.2f} | ms/batch {ms_per_batch:5.2f} | '
                  f'loss {cur_loss:5.2f} | ppl {ppl:8.2f}')
            total_loss = 0
            start_time = time.time()
