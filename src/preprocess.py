from Helpers import *
import os
from pathlib import Path
import pickle
import json
import torch
import nltk
from nltk.stem.porter import PorterStemmer
import json
import spacy
from nltk.corpus import stopwords
import numpy as np
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
import random
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset
from typing import Tuple
import re
import copy
import time
import itertools
from itertools import zip_longest
import math
import tqdm
import sys

#Parse through all the questions and respective answers and create list of
#dictionaries containing all the information we need
path = os.path.dirname(os.path.realpath(__file__)[:-3])
path = Path(path).parent
print(path)
with open(str(path) + '/data/dev-v2.0.json', 'r', encoding = 'utf-8') as f:
    raw_data = json.load(f)
    examples = []
    num = 1
    for document in raw_data['data']:
        for paragraph in document['paragraphs']:
            context = paragraph['context']
            context_p = tokenize_sub(context)
            for qna in paragraph['qas']:
                question = qna['question']
                question_p = tokenize_sub(question)
                id_ = qna['id']
                label = 1 if qna.get("is_impossible", False) else 0
                answers, answers_t, answer_starts, answer_ends = [], [], [], []
                for answer in qna['answers']:
                    answer_start = answer['answer_start']
                    answer_end = answer_start + len(answer['text'])
                    answers.append(answer['text'])
                    answer_starts.append(answer_start)
                    answer_ends.append(answer_end)
                    answers_t.append(tokenize_sub(answer['text']))
                example = {
                    'id': id_,
                    'idx': len(examples),
                    'context': context,
                    'question': question,
                    'context_p': context_p,
                    'question_p': question_p,
                    'impossible': label,
                    'answers': answers,
                    'answers_t': answers_t,
                    'answer_starts': answer_starts,
                    'answer_ends': answer_ends,
                    'mask': create_mask(answer_starts, answer_ends, answers, context, context_p)
                    }
                examples.append(example)


#For the empty masks of the impossible answers or for the masks that the answer was not found
#in the processed context body, alter them as all zeros up to the processed context length
for example in examples:
    if len(example['mask']) == 0:
        example['mask'] = '0' * len(example['context_p']) 
    for pos, mask in enumerate(example['mask']):
        if not mask[0].isdecimal():
            example['mask'][pos] = '0' * len(example['context_p'])

#Create vocabulary dictionary, word list and work to index dictionary
vocab = {}
for example in examples:
    for word in example['context_p']:
        if word not in vocab:
            vocab[word] = 1 
        else:
             vocab[word] += 1
    for word in example['question_p']:
        if word not in vocab:
            vocab[word] = 1
        else:
            vocab[word] += 1
    for answer in example['answers_t']:
        for word in answer:
            if word not in vocab:
                vocab[word] = 1 
            else:
                vocab[word] += 1
word_list = ['_UNK_'] + sorted(vocab, key = vocab.get, reverse = True)
word_idx_lookup = dict([(x,y) for (y,x) in enumerate(word_list)])


#save all necessary files
output_file = open(str(path) + '/data/example.txt', 'w', encoding='utf-8')
output_file.write(str(examples))
output_file.close()

word_list_file = open(str(path) + "/data/word_list.pkl", "wb")
pickle.dump(word_list, word_list_file)
word_list_file.close()

vocab_file = open(str(path) + "/data/vocab.pkl", "wb")
pickle.dump(vocab, vocab_file)
vocab_file.close()

word_idx_lookup_file = open(str(path) + "/data/word_idx_lookup.pkl", "wb")
pickle.dump(word_idx_lookup, word_idx_lookup_file)
word_idx_lookup_file.close()
