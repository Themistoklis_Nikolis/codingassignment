import numpy as np
import torch
import nltk
from nltk.stem.porter import PorterStemmer
import json
import spacy
from nltk.corpus import stopwords
import numpy as np
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
import random
from torch import nn, Tensor
import torch.nn.functional as F
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.utils.data import dataset
from typing import Tuple
import re
import copy
import time
import itertools
from itertools import zip_longest, repeat, chain, islice
import math
import tqdm
import sys

nltk.download('omw-1.4')

def chunker_longest(iterable, chunksize):
    """This function combined with range() allows for the parsing of a list
    chunksize elements at a time while moving. This combined with a for loop
    in range of chunksize allow to parse the entirety of list elements
    by chunksize!
        
    Utility: to find processed answer in the processed context in order to create the mask
    
    Inputs:
        :iterable: An iterable object, in our case a list of strings of the processed context
        :type iterable: iter object
        :chunksize: The step value for parsing, in our case the length of the processed answer
        :type chunksize: int
        
    Outputs:
        chunk of list elements
        
    Example:
        :input: iterable = [1, 2, 3, 4], chunksize = 2
        :output: [1,2], [3,4]"""
    return zip_longest(*[iter(iterable)] * chunksize)

def tokenize(sentence):
    """This helper function tokenizes and splits a string (sentence) to tokens based
    on the natural language toolkit tokenizer 'omw-1.4'. This tokenizer was preferred 
    for various reasons, one of many, possibly the greatest, is that the tokenization of ""3.88$""
    is: ""3.88"" + ""$"", instead of  ""3"" + ""."" + ""88"" + ""$"", which is the tokenization of another
    famous tokenizer: punct.
    
    Utility: tokenize sentence

    Inputs:
        :sentence: the sentence we want to tokenize
        :type sentence: string

    Outputs:
        :tokenized sentence
        :type outputs: list of strings
    """

    return nltk.word_tokenize(sentence)

def lemmatize(phrase):
    """This helper function lemmatizes a string (sentence) to lemmas based
    on the spacy basic English lemmatizer. Between stemming and lemmatization the latter
    is considered the best practice in general, since stemming creates non-existing words.
    The function was not used eventually because I did not find to not sacrifice meaning
     of the answer while having a smaller vocabulary.
    
    Utility: lemmatize sentence

    Inputs:
        :sentence: the sentence we want to lemmatize
        :type sentence: string

    Outputs:
        :lemmatized sentence
        :type outputs: string
    """
    return " ".join([word.lemma_ for word in sp(phrase)])

def tokenize_sub(sentence):
    """This function is used to process each sentence of the context, question and answer.
    It removes dashes, minus signs and "'s" that pose many problems in finding the correct answer.
    Furthermore, all sentences are lowered, tokenized and stripped of punctuation marks.
    
    Utility: process raw text
    
    Inputs:
        :sentence: the sentence we want to lemmatize
        :type sentence: string

    Outputs:
        :processed sentence
        :type outputs: list of strings
    """
    sentence = sentence.replace("—",' ')
    sentence = sentence.replace("-",' ')
    sentence = sentence.replace("'s",'')
    lwrd_sbd = re.sub("[():;'.,!?\\-]", '', sentence.lower()).split('\n')
    tknz = tokenize(lwrd_sbd.pop())
    return tknz

# def process(sentences):
#     lwrd_sbd = re.sub("[():;'.,!?\\-]", '', sentences.lower()).split('\n')
#     tknz = tokenize(lwrd_sbd.pop())
#     lmm = [lemmatize(tkn) for tkn in tknz]
#     return lmm

def create_mask(answer_starts, answer_ends, answers, context, context_p):
    """This function is used to create a mask of 0s and 1s with length equal to context length,
    with the ones indicating the answer starts and answer ends in the processed context. This way the 
    impossible answers present, are assigned a mask of all zeros, so that we bypass the need for a question
    classifier. If the output of the model is all zeros that will mean, that it considers the question
    asked based on the given context as impossible.
    
    Utility: create answer mask
    
    Inputs:
        :answer_starts: start of the answers (in characters)
        :type answer_starts: int
        :answer_ends: end of the answers (in characters)
        :type answer_ends: int
        :answers: the answers 
        :type answer_starts: string
        :context: context of the answer
        :type context: string
        :answer context_p: processed context of the answer
        :type context_p: list of strings

    Outputs:
        :answer mask
        :type outputs: list of strings

    Example:
        Inputs:
            :answer_starts = 0
            :answer_ends = 1
            :answer = "The Duke"
            :context = "The Duke answers the phone"
            :context_p = ["the", "duke", "answers", "the", "phone"]
        Outputs:
            :mask = "11000"
    """
    masks = []
    for (an_start, an_end, answer) in zip(answer_starts, answer_ends, answers):
        mask = []
        for pos, token in enumerate(context):
            if pos >= int(an_start) and pos <= int(an_end):
                mask.append(token)
        mask = "".join(mask)
        tmask = tokenize_sub(answer)
        j = 0
        if tmask is not None: 
            for j in range(len(tmask)):
                k = 0
                for chunk in chunker_longest(context_p[j:], len(tmask)):
                    i = k*len(tmask) + j
                    k += 1
                    if (np.array(chunk) == np.array(tmask)).all():
                        mask = ['0' * i + '1' * len(tmask) + '0' * (len(context_p) - len(tmask) - i)]
                        break
                j += 1
        masks.append(mask)
    return masks

def get_train_indices(examples, mcl, mql):
    """
    Description of get_train_indices: From all the examples keep the indices that
    are up to max question length and max context length.

    Inputs:
        examples: dictionaries containing context, question and answers
        type: examples: list of dictionaries

    Outputs:
        indices: indices of examples
        type: indices: numpy series of integers
    """
    contextLengths = []
    questionLengths = []
    for example in examples:
        contextLengths.append(len(example['context_p']))
        questionLengths.append(len(example['question_p']))
    contextLengths = np.array(contextLengths)
    questionLengths = np.array(questionLengths)
    indices = np.nonzero(contextLengths <= mcl)[0]
    qIndices = np.nonzero(questionLengths <= mql)[0]
    indices = np.intersect1d( indices, qIndices )
    return indices

def lineToWordIndices(word_idx_lookup, context_p):
    """
    Description of lineToWordIndices: Return list of indices of the processed context from the
     sorted set of the vocabulary. If the word in the context is not present in the vocabulary then
     0 is assigned as an index and the number of unknown words is stored in a global variable.

    Inputs:
        word_idx_lookup: dictionary of the words and their unique index
        type: word_idx_lookup: dictionary
        context_p: processed context
        type: list of strings

    Outputs:
        indices: indices of words in context
        type: indices: list of integers
    """
    indices = []
    global num_uknwn
    for word in context_p:
        if word in word_idx_lookup:
            indices.append(word_idx_lookup[word])
        else:
            num_uknwn += 1
            indices.append(0)
    return indices



def trimmer(seq, size, filler = 0):
    """
    Description of trimmer: trim list to specific length. If list is shorter then fill with filler

    Inputs:
        seq: list to trim to size
        type: seq: list
        size: size to trim list 
        type: size: int
        filler = 0: value to fill up to size
        type: filler: whatever"""
    
    return islice(chain(seq, repeat(filler)), size)

def train_val_creation(examples, word_idx_lookup, indices, mcl, mql, path, val = ''):
    """
    Description of train_val_creation: Create train set or evaluation set based
    on indices. If context or question exceed maximum limit values they are truncated, 
    if they are shorter, then they are filled up to maximum length.

    Args:
        examples (list of dictionaries): Each element is a (x, y) pair
        word_idx_lookup (dictionary): Dictionary of words to index
        indices (array of integers): which indices of the whole set to keep
        mcl (int): maximum context length
        mql (int): maximum question length
        val = '' (string): suffix to filename to save context, question and answer

        """
    context_ar, question_ar, mask_ar = [], [], []
    for example in examples:
        if example['idx'] in indices:
            context_line = list(trimmer(lineToWordIndices(word_idx_lookup, example['context_p']), int(mcl)))
            
            question_line = list(trimmer(lineToWordIndices(word_idx_lookup, example['question_p']), int(mql)))
            
            for mask in example['mask']:
                mask_line = [list(map(int, list(mask[0].strip())))]
            mask_line = list(trimmer(mask_line[0], int(mcl)))
            
            context_ar.append(context_line)
            question_ar.append(question_line)
            mask_ar.append(mask_line) 

    context_ar = np.array(context_ar)
    question_ar = np.array(question_ar)
    mask_ar = np.array(mask_ar)     

    np.save(path + '/data/context_ar' + val + '.npy', context_ar)
    np.save(path + '/data/question_ar' + val + '.npy', question_ar)
    np.save(path + '/data/mask_ar' + val + '.npy', mask_ar)   

class TransformerModel(nn.Module):

    def __init__(self, ntoken: int, d_model: int, nhead: int, d_hid: int,
                 nlayers: int, dropout: float = 0.5):
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.encoder = nn.Embedding(ntoken, d_model)
        self.d_model = d_model
        self.decoder = nn.Linear(d_model, ntoken)

        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src: Tensor, src_mask: Tensor) -> Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        src = self.encoder(src) * math.sqrt(self.d_model)
        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, src_mask)
        output = self.decoder(output)
        return output


def generate_square_subsequent_mask(sz: int) -> Tensor:
    """Generates an upper-triangular matrix of -inf, with zeros on diag."""
    return torch.triu(torch.ones(sz, sz) * float('-inf'), diagonal=1)

class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x: Tensor) -> Tensor:
        """
        Args:
            x: Tensor, shape [seq_len, batch_size, embedding_dim]
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)

def data_process(raw_text_iter: dataset.IterableDataset) -> Tensor:
    """Converts raw text into a flat Tensor."""
    data = [torch.tensor(vocab(tokenizer(item)), dtype=torch.long) for item in raw_text_iter]
    return torch.cat(tuple(filter(lambda t: t.numel() > 0, data)))

def batchify(data: Tensor, bsz: int) -> Tensor:
    """Divides the data into bsz separate sequences, removing extra elements
    that wouldn't cleanly fit.

    Args:
        data: Tensor, shape [N]
        bsz: int, batch size

    Returns:
        Tensor of shape [N // bsz, bsz]
    """
    seq_len = data.size(0) // bsz
    data = data[:seq_len * bsz]
    data = data.view(bsz, seq_len).t().contiguous()
    return data.to(device)

def get_batch(source: Tensor, i: int) -> Tuple[Tensor, Tensor]:
    """
    Args:
        source: Tensor, shape [full_seq_len, batch_size]
        i: int

    Returns:
        tuple (data, target), where data has shape [seq_len, batch_size] and
        target has shape [seq_len * batch_size]
    """
    seq_len = min(bptt, len(source) - 1 - i)
    data = source[i:i+seq_len]
    target = source[i+1:i+1+seq_len].reshape(-1)
    return data, target

def train(model: nn.Module) -> None:
    model.train()  
    total_loss = 0.
    log_interval = 200
    start_time = time.time()
    src_mask = generate_square_subsequent_mask(bptt).to(device)

    num_batches = len(train_data) // bptt
    for batch, i in enumerate(range(0, train_data.size(0) - 1, bptt)):
        data, targets = get_batch(train_data, i)
        batch_size = data.size(0)
        if batch_size != bptt:  
            src_mask = src_mask[:batch_size, :batch_size]
        output = model(data, src_mask)
        loss = criterion(output.view(-1, ntokens), targets)

        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        if batch % log_interval == 0 and batch > 0:
            lr = scheduler.get_last_lr()[0]
            ms_per_batch = (time.time() - start_time) * 1000 / log_interval
            cur_loss = total_loss / log_interval
            ppl = math.exp(cur_loss)
            print(f'| epoch {epoch:3d} | {batch:5d}/{num_batches:5d} batches | '
                  f'lr {lr:02.2f} | ms/batch {ms_per_batch:5.2f} | '
                  f'loss {cur_loss:5.2f} | ppl {ppl:8.2f}')
            total_loss = 0
            start_time = time.time()